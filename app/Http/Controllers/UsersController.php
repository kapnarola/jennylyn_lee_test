<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\User;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function finduser(Request $request)
    {
        # code...
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if($user)
        {
            $auth_tokan_gen = rand();
            User::where('email', $user->email)->update(array('auth_tokan' => $auth_tokan_gen));
            $d = [ 
                'title' => 'Login Link',
                'email' => $user->email,
                'name'  => $user->name,
                'auth_tokan' => $auth_tokan_gen
             ];

            $user_name=$user->name;
            $user_email=$user->email;
            Mail::send('emails.loginlink', $d, function($message) use($user_name,$user_email) {
                
            $message->to($user_email, $user_name)->subject('Login Link Request');

            });
            Session::flash('Invalid','Login Link Send Your Email. Please Check your inbox.');
            return redirect()->back();   
        }
        else
        {   
            Session::flash('Invalid','Invalid Credentials..!');
            return redirect()->back();
        }
    }

    public function login($auth_tokan)
    {
        # code...
        $finduser = User::where('auth_tokan', $auth_tokan)->first();
        if($finduser)
        {
            User::where('email', $finduser->email)->update(array('auth_tokan' => ""));
            Session::put('username',$finduser->email);
            return view('welcome');
        }
        else
        {
            Session::flash('Invalid','Login Link Expired..!');
            return view('login_request');
        }
    }

    public function logout() {
        Session::flush();
        return Redirect('/');
    }
}
